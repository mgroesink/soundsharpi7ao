﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SoundsharpUI.Startup))]
namespace SoundsharpUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
